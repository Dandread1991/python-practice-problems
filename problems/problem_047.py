# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):

    if len(password) > 12 or len(password) < 6:
            return False
    else:

          check_special = False
          check_upper = False
          check_lower = False
          check_digit = False

          for ch in password:
                if ch.isalpha():
                      if ch.isupper():
                            check_upper = True
                      elif ch.islower():
                            check_lower = True
                elif ch.isdigit():
                      check_digit = True

                elif ch == "!" or ch == "$" or ch =="@":
                      check_special = True

          return check_upper and check_digit and check_lower and check_special


#test = "3rT3yyyy"
#print(check_password(test))
