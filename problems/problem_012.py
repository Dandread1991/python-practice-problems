# Complete the fizzbuzz function to return
# * The word "fizzbuzz" if number is evenly divisible by
#   by both 3 and 5
# * The word "fizz" if number is evenly divisible by only
#   3
# * The word "buzz" if number is evenly divisible by only
#   5
# * The number if it is not evenly divisible by 3 nor 5
#
# Try to combine what you have done in the last two problems
# from memory.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
import problem_010
import problem_011

def fizzbuzz(number):

    a = problem_010.is_divisible_by_3(number)
    b = problem_011.is_divisible_by_5(number)
    c=number
    if isinstance(a,str) and isinstance(b,str):
        return a+b
    elif isinstance(a,str) and isinstance(b,int):
        return a
    elif isinstance(a,int) and isinstance(b,str):
        return b
    else:
        return number
