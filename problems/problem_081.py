# Write four classes that meet these requirements.
#
# Name:       Animal
#
# Required state:
#    * number_of_legs, the number of legs the animal has
#    * primary_color, the primary color of the animal
#
# Behavior:
#    * describe()       # Returns a string that describes that animal
#                         in the format
#                                self.__class__.__name__
#                                + " has "
#                                + str(self.number_of_legs)
#                                + " legs and is primarily "
#                                + self.primary_color
#
#
# Name:       Dog, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Bark!"
#
#
#
# Name:       Cat, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Miao!"
#
#
#
# Name:       Snake, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Sssssss!"


class Animal:
    def __init__(self,num_legs,color) -> None:
        self.num_legs = num_legs
        self.color = color

    def describe(self):
        return f"{self.__class__.__name__} has {str(self.num_legs)} legs and is primarily {self.color}"

class Dog(Animal):
    def __init__(self, num_legs, color) -> None:
        super().__init__(num_legs, color)

    def speak(self):
        return "Bark!"

class Cat(Animal):
    def __init__(self, num_legs, color) -> None:
        super().__init__(num_legs, color)

    def speak(self):
        return "Meow"

class Snake(Animal):
    def __init__(self, num_legs, color) -> None:
        super().__init__(num_legs, color)

    def speak(self):
        return "Sssssss"

#test1 = Animal(2,"black")
#test2 = Dog(4,"Spotted")
#test3 = Cat(4,"purple")
#test4 = Snake(0,"green")

#print(test1.describe())
#print(test2.describe())
#print(test3.describe())
#print(test4.describe())


#print(test2.speak())
#print(test3.speak())
#print(test4.speak())
