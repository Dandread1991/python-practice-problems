# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    if not s:
        return ""
    else:
        dict_str = {}
        new_str = ""
        for index,elt in enumerate(s):
            if elt not in dict_str:
                dict_str[elt] = index
    for key_val in dict_str.keys():
        new_str+=key_val
    return new_str

#test = "abccbad"
#print(remove_duplicate_letters(test))
