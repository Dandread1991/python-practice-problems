# Write a function that meets these requirements.
#
# Name:       temperature_differences
# Parameters: highs: a list of daily high temperatures
#             lows: a list of daily low temperatures
# Returns:    a new list containing the difference
#             between each high and low temperature
#
# The two lists will be the same length
#
# Example:
#     * inputs:  highs: [80, 81, 75, 80]
#                lows:  [72, 78, 70, 70]
#       result:         [ 8,  3,  5, 10]

#first try
def temperature_differences(highs,lows):
    lst = []
    i=0
    for num in highs:
        lst.append(num-lows[i])
        i+=1
    return lst

test = [80,81,75,80],[72,78,70,70]
print(temperature_differences(*test))

#better solution

def better_temp(highs,lows):
    lst = []
    for high,low in zip(highs,lows):
        lst.append(high-low)
    return lst

test = [80,81,75,80],[72,78,70,70]
print(better_temp(*test))
