# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):

    if not values:
        return None
    max_list = sorted(values)
    n = len(values)
    return max_list[n-1]
