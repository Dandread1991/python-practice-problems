# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(lst):
    n = len(lst)
    lst1,lst2= [],[]
    if n %2 != 0:
        for i in range((n//2)+1):
            lst1.append(lst[i])
        for j in range((n//2)+1,n):
            lst2.append(lst[j])
    else:
        for i in range(n//2):
            lst1.append(lst[i])
        for j in range((n//2),n):
            lst2.append(lst[j])

    return lst1,lst2

#test = [1,2,3]
#print(halve_the_list(test))
