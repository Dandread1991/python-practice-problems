# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    if not values:
        return None
    else:
        total = 0
        for elt in values:
            total+=elt
        return total
